<?php
require "app/User/Login.php";
require "app/User/loggedIn.php";
$Login = new Login();
$RedisConnector = new RedisConnector();

$RedisConnector->redisConnect($redisInfo);

loggedIn();

if (isset($_POST['login'])) {
    $username = $_POST["username"];
    $password = $_POST["password"];
    $errormsg = $Login->userLogin($username, $password, $redis);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" >
<body>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 well">
            <form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="loginform">
                <fieldset>
                    <legend>Login</legend>

                    <div class="form-group">
                        <label for="name">Username</label>
                        <input type="text" name="username" placeholder="Username" required class="form-control" />
                    </div>

                    <div class="form-group">
                        <label for="name">Password</label>
                        <input type="password" name="password" placeholder="Your Password" required class="form-control" />
                    </div>

                    <div class="form-group">
                        <input type="submit" name="login" value="Login" class="btn btn-primary" />
                    </div>
                </fieldset>
            </form>
            <span class="text-danger"><?php if (isset($errormsg)) { echo $errormsg; } ?></span>
        </div>
    </div>
</div>

</body>
</html>