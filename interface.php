<?php
session_start();
print_r($_SESSION);
//TODO: delet this
header("Access-Control-Allow-Origin: *");
if(isset($_SESSION["username"])) {
    require "app/Database/Redis.php";

    $RedisConnector = new RedisConnector();
    $RedisConnector->redisConnect($redisInfo);

    require "app/Message/NewMessage.php";
    require "app/Message/DeleteMessage.php";
    require "app/Message/ListMessages.php";

    $NewMessage = new NewMessage();
    $DeleteMessage = new DeleteMessage();
    $ListMessage = new ListMessages();


    if (isset($_GET["sendMessage"])) {
        $NewMessage->push($_GET["username"], $_GET["content"], $redis);
    }

    if (isset($_GET["deleteMessage"])) {
        $return = $DeleteMessage->delMessage($_GET["id"], $redis);
        if ($return === "1337") {
            echo "<h1>1337</h1>";
        } elseif ($return) {
            echo "<p>Message deleted successfully</p>";
        } else {
            echo "<p>Something went wrong, please try again later</p>";
        }
    }

    if (isset($_GET["listMessages"])) {
        echo $ListMessage->list($redis);
    }
}