<?php
require "app/User/Register.php";
require "app/User/loggedIn.php";

loggedIn();

$error = false;
$RedisConnector = new RedisConnector();
$Register = new Register();
$RedisConnector->redisConnect($redisInfo);

if (isset($_POST['signup'])) {
    $username = $_POST["username"];
    $password = $_POST["password"];
    $cpassword = $_POST["cpassword"];
    $return = $Register->userRegister($username, $password, $cpassword, $redis);
}

if (isset($return)) {
    if (is_array($return)) {
        $errors = $return;
    } else {
        $successmsg = $return;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Register</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" >
<body>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 well">
            <form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="signupform">
                <fieldset>
                    <legend>Sign Up</legend>

                    <div class="form-group">
                        <label for="name">Username</label>
                        <input type="text" name="username" placeholder="Username" required value="<?php if($error) echo $username; ?>" class="form-control" />
                        <span class="text-danger"><?php if (isset($errors["usernameError"])) { echo $errors["usernameError"]; } ?></span>
                    </div>

                    <div class="form-group">
                        <label for="name">Password</label>
                        <input type="password" name="password" placeholder="Password" required class="form-control" />
                        <span class="text-danger"><?php if (isset($errors["passwordError"])) { echo $errors["passwordError"]; } ?></span>
                    </div>

                    <div class="form-group">
                        <label for="name">Confirm Password</label>
                        <input type="password" name="cpassword" placeholder="Confirm Password" required class="form-control" />
                        <span class="text-danger"><?php if (isset($errors["cpasswordError"])) { echo $errors["cpasswordError"]; } ?></span>
                    </div>

                    <div class="form-group">
                        <input type="submit" name="signup" value="Sign Up" class="btn btn-primary" />
                    </div>
                </fieldset>
            </form>
            <span class="text-danger"><?php if (isset($errors["userExists"])) { echo $errors["userExists"]; }?></span>
            <span class="text-success"><?php if (isset($successmsg)) { echo $successmsg; } ?></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center">
            Already Registered? <a href="login.php">Login Here</a>
        </div>
    </div>
</div>

</body>
</html>