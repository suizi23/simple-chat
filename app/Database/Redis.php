<?php
require "vendor/autoload.php";
require "app/Config/config.php";
Predis\Autoloader::register();

class RedisConnector
{
    function redisConnect($redisInfo)
    {
        try {
            global $redis;
            $redis = new Predis\Client(array(
                "scheme" => $redisInfo["scheme"],
                "host" => $redisInfo["host"],
                "port" => $redisInfo["port"],
                "password" => $redisInfo["password"]));
        } catch
        (Exception $exception) {
            die("Connection to Database failed");
        }
    }
}