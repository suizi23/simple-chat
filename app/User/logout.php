<?php
session_start();

function logOut() {
    // If session is set, nuke it.
    if(isset($_SESSION['username'])) {
        session_destroy();
        unset($_SESSION['username']);
        header("Location: info.html");
    } else {
        header("Location: index.html");
    }
}