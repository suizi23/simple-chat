<?php
session_start();

require "app/Database/Redis.php";

class Login {

    function userLogin($username, $password, $redis) {
        $key = "user:" . strtolower($username);
        if($redis->exists($key) === 1) {
            $result = $redis->get($key);
            if (password_verify($password, $result)) {
                $_SESSION['username'] = $username;
                header("Location: dashboard.php");
                echo "testing2";
            }
        } else {
            return "Incorrect Username or Password!";
        }
    }

}