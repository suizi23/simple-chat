<?php
session_start();

require "app/Database/Redis.php";

class Register {

    function userRegister($username, $password, $cpassword, $redis) {
        $errors = array();
        $error = false;
        $hashedpassword = password_hash($password, PASSWORD_DEFAULT);

        // Username can only contain alphanumeric characters, - _ .
        if (!preg_match("/^[a-zA-Z0-9_.-]+$/", $username)) {
            $error = true;
            $errors["usernameError"] = "Username can only be letters, dash, point, underscore and numbers.";
        }

        if (strlen($password) < 8) {
            $error = true;
            $errors["passwordError"] = "Password must be at least 8 characters long.";
        }
        if ($password != $cpassword) {
            $error = true;
            $errors["cpasswordError"] = "Passwords don't match.";
        }
        if (!$error) {
            $key = "user:" . strtolower($username);
            if($redis->exists($key) === 0) {
                $redis->set($key, $hashedpassword);
                return "Successfully registered!";
            } else {
                return "Username is taken";
            }
        } else {
            return $errors;
        }
    }

}