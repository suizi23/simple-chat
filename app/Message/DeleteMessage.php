<?php

class DeleteMessage {
    function delMessage($id, $redis) {
        if (is_numeric($id)) {
            if($redis->lset("messages", $id, "deleted") == "OK") {
                return true;
            }
        } else {
            return "1337";
        }
        return false;
    }
}