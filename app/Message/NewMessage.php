<?php

class NewMessage {
    function push ($username, $content, $redis) {
        $id = $redis->llen("messages");
        $serializeddata = serialize(array(
            "username" => $username,
            "content" => $content,
            "id" => $id,
            "date" => date("Y-m-d-h-i-s"),
        ));
        $redis->rpush("messages", $serializeddata);
    }
}